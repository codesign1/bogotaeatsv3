cordova build --release android

cd ..

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore bogotaeats-key.keystore bogotaeatsv3/platforms/android/build/outputs/apk/android-release-unsigned.apk estudiocodesign

cd bogotaeatsv3/platforms/android/build/outputs/apk/

/Users/pipe/Library/Android/sdk/build-tools/25.0.2/zipalign -v 4 android-release-unsigned.apk bogotaeats.apk