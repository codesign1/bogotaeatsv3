angular.module('bogotaEats.controllers', ['bogotaEats.services'])

/**
 * Controller for the SignUp template
 */
   .controller('SignUpCtrl', function ($scope, AuthService, $location, $ionicHistory, $state, $ionicSlideBoxDelegate, $http, $ionicPopup) {
    $scope.options = {
      loop: false,
      effect: 'slide',
      speed: 500,
      pagination: false
    };

    $http.get("https://app.bogotaeats.com/api/terminos").then(function (res) {
      console.log(res);
      $scope.termino = res.data[0];
    });

    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
      // data.slider is the instance of Swiper
      $scope.slider = data.slider;
    });

    $scope.$on("$ionicSlides.slideChangeStart", function (event, data) {
      console.log('Slide change is beginning');
    });

    $scope.$on("$ionicSlides.slideChangeEnd", function (event, data) {
      // note: the indexes are 0-based
      $scope.activeIndex = data.slider.activeIndex;
      $scope.previousIndex = data.slider.previousIndex;
    });

     $scope.nextSlide = function() {
       $ionicSlideBoxDelegate.next();
     };

     $scope.prevSlide = function() {
       $ionicSlideBoxDelegate.previous();
     };

    // Form data for the login modal
    $scope.loginData = {};

    $scope.signUp = function () {
      AuthService.signup($scope.loginData.username, $scope.loginData.password);
    };

    $scope.logIn = function () {
      AuthService.login($scope.loginData.username, $scope.loginData.password);
    };

    $scope.loginWithFacebook = function () {
      AuthService.loginWithFacebook();
    };

    $scope.loginWithGoogle = function () {
      AuthService.loginWithGoogle();
    };

    $scope.continueWithoutRegistration = function () {
      localStorage.setItem('authorized', 'true');

      // make sure there's not a back button on redirect
      $ionicHistory.clearHistory();
      $location.path('/app/newsfeed');
    }

     $scope.showTerminos = function () {
       $scope.filterPopup = $ionicPopup.show({
         title: $scope.termino.nombre,
         templateUrl: 'templates/popups/terminos.html',
         cssClass: 'filters-popup',
         scope: $scope
       });
     };

    $scope.hideTerminos = function () {
      $scope.filterPopup.close();
    }
  })

  .controller('AppCtrl', function ($scope, $ionicModal, $timeout, AuthService, $location) {

    $scope.isLoggedIn = function () {
      return AuthService.isLoggedIn();
    };

    $scope.isAdmin = function () {
      return window.localStorage.tipo=="admin";
    };

    $scope.logOut = function () {
      localStorage.clear();
      $location.path('/sign-up');
    };

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:

    $scope.init = function () {
      var authorized = localStorage.getItem('authorized');
      if (!authorized) {
        $location.path('/sign-up');
      } else {
        $location.path('/');
      }
    };

    $scope.init();
  })

  .controller('NewsfeedCtrl', function ($scope, DataService, $location, $ionicPopup, FilterService, $ionicLoading, $cordovaGeolocation, DistanceService, $ionicScrollDelegate, $timeout) {
    $ionicLoading.show({
      template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
    });

    $scope.$on('$ionicView.enter', function (e) {$scope.selectedFilters()});

    window.sessionStorage['filters'] = 0;

    $scope.searchParam = "";

    $scope.doRefresh = function() {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      getData();
    };

    $scope.selectedOrder = 'fecha';
    $scope.reverseOrder = true;

    DataService.getFavorites();

    $scope.navigateTo = function (id) {
      $location.path('/app/reviews/'+id);
    };

    function loadApp() {
      var reviews;
      if(reviews){
        DataService.getAmountReviews().then(function (res) {
          if(res.count > reviews.length){
            getData();
          }else{
            $scope.reviews = reviews;

            $cordovaGeolocation.getCurrentPosition().then(function (res) {
              angular.forEach($scope.reviews, function (review) {
                review.distanceTo = parseFloat(DistanceService.getDistance(res.coords.latitude, res.coords.longitude,
                  review.geoloc.lat, review.geoloc.lng, 'K').toFixed(1));
              });
            });
            $ionicLoading.hide();
          }
        })
      }else{
        getData();
      }
    }

    loadApp();

    function getData() {
      DataService.getNewsfeed().then(function (res) {
        $scope.reviews = res.data.news;
        $cordovaGeolocation.getCurrentPosition().then(function (res) {
          angular.forEach($scope.reviews, function (review) {
            review.distanceTo = parseFloat(DistanceService.getDistance(res.coords.latitude, res.coords.longitude,
              review.geoloc.lat, review.geoloc.lng, 'K').toFixed(1));
          })
        });
        $ionicLoading.hide();
      });
      DataService.getBannersActuales().then(function (res) {
        $scope.banners = res.data.Banner;
        $scope.idBanner = 0;
        $timeout(cambiarBanner, 10000);
      })
    }

    $scope.esconderBanner = function () {
      $scope.hideBanner = true;
    };

    function cambiarBanner() {
      if($scope.idBanner == $scope.banners.length-1){
        $scope.idBanner = 0;
      }else{
        $scope.idBanner++;
      }
      $timeout(cambiarBanner, 10000);
    }

    $scope.onSwipe = function (direction) {
      console.log(direction);
    };

    $scope.toggleFavorite = function (reviewId) {
      DataService.toggleFavorite(reviewId);
    };

    $scope.isFavorite = function (reviewId) {
      return DataService.isFavorite(reviewId);
    };

    $scope.selectOrder = function (order) {
      if ($scope.selectedOrder === order) {
        $scope.reverseOrder = !$scope.reverseOrder;
      } else {
        $scope.selectedOrder = order;
        if(order == "distanceTo"){
          $scope.reverseOrder = false;
        }
        else{
          $scope.reverseOrder = true;
        }
      }
    };

    $scope.openMap = function () {
      $location.path('/app/map')
    };

    /**
     * Filter popup related
     */

    $scope.showFilters = function () {
      $scope.filterPopup = $ionicPopup.show({
        title: 'Filtrar',
        templateUrl: 'templates/popups/filters.html',
        cssClass: 'filters-popup',
        scope: $scope
      })

    };

    $scope.hideFilters = function () {
      $scope.filterPopup.close();
      $scope.selectedFilters();
      $ionicScrollDelegate.scrollTop(true);
    };

    $scope.filters = FilterService.filters;


    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };

    $scope.filterByName = function (searchParam) {
      return function (item) {
        var name = item.nombre.toUpperCase();
        var search = searchParam.toUpperCase();
        return name.includes(search);
      }
    };

    $scope.selectedFilters = function () {
      $scope.filtros = 0;
      var tipoCom = $scope.filters.tipoComida.values;
      var tipoOca = $scope.filters.tipoOcasion.values;
      var zona = $scope.filters.zona.values;
      tipoCom.forEach(function (tipo) {
        if(tipo.selected){
          $scope.filtros ++;
        }
      });
      tipoOca.forEach(function (tipo) {
        if(tipo.selected){
          $scope.filtros ++;
        }
      });
      zona.forEach(function (tipo) {
        if(tipo.checked){
          $scope.filtros ++;
        }
      });
    };

    $scope.clearFilters = function () {
      var tipoCom = $scope.filters.tipoComida.values;
      var tipoOca = $scope.filters.tipoOcasion.values;
      var zona = $scope.filters.zona.values;
      tipoCom.forEach(function (tipo) {
        tipo.selected = false;
      });
      tipoOca.forEach(function (tipo) {
        tipo.selected = false;
      });
      zona.forEach(function (tipo) {
        tipo.checked = false;
      });
    };

    $scope.markSelected = function (name, catName) {
      var cat = $scope.filters[catName].values;
      cat.forEach(function (cat) {
        if(cat.nombre != name){
          cat.selected = false;
        }
      })
    }



  })

  .controller('MapCtrl', function ($scope, DataService, $cordovaGeolocation, $ionicPopup, FilterService, $ionicLoading, $filter, $location, $timeout) {
    $scope.filters = FilterService.filters;
    $scope.onReadySwiper = function(swiper){
      swiper.initObservers();
    };

    $scope.$watch('filters', function (newVal) {
      $scope.reviews = $filter('bogotaEats')($scope.realReviews);
      $timeout(function() {
        $scope.calculateCarouselReviews();
      });
      $scope.addMarkers($scope.reviews);
    }, true);

    $scope.mapMarkers = {};

    // the displayReviews will change depending on what the user selected as a location filter
    $scope.selectedLocation = '';
    $scope.currentPosition = null;

    $scope.carouselReviews = [];

    $scope.zones = {};

    $scope.$on('$ionicView.enter', function (e) {
      $scope.selectedFilters();
      if(!$scope.realReviews) {
        $ionicLoading.show({
          template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
        });

        Promise.all([DataService.getNewsfeed(), $cordovaGeolocation.getCurrentPosition()])
          .then(function (res) {
            $scope.realReviews = res[0].data.news;
            $scope.reviews = $filter('bogotaEats')($scope.realReviews);

            angular.forEach($scope.reviews, function (review) {
              $scope.zones[review.zona] = true;
            });
            $scope.$apply(function () {
              $scope.zones = Object.keys($scope.zones);
            });

            $scope.currentPosition = res[1].coords;

            var mapOptions = {
              zoom: 15,
              center: {lat: $scope.currentPosition.latitude, lng: $scope.currentPosition.longitude},
              disableDefaultUI: true
            };

            $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

            $scope.calculateCarouselReviews($scope.reviews);

            // add a listener for when the bounds change, so that we can check which reviews are in view and show them in
            // the bottom carousel
            $scope.map.addListener('bounds_changed', function () {
              $scope.calculateCarouselReviews();
            });

            $scope.addMarkers($scope.reviews);
            $ionicLoading.hide();
          });
      }
    });

    $scope.addMarkers = function (reviews) {
      // clear the map reference on all existing markers
      var image = {
        url: 'img/PinMap.png',
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(40, 51),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 51)
      };
      angular.forEach($scope.mapMarkers, function (marker) {
        marker.setMap(null);
      });
      $scope.mapMarkers = {};
      $scope.infoMarkers ={};

      angular.forEach(reviews, function (review) {
        var contentString = '<div id="content">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<h3 id="firstHeading" class="firstHeading">'+review.nombre+'</h3>'+
          '<div id="bodyContent">'+
          '<p>B.E. :'+review.calificacion+'</p>'+
          '</div>'+
          '</div>';
        $scope.infoMarkers[review.id] =  new google.maps.InfoWindow({
          content: contentString
        });
        $scope.mapMarkers[review.id] = new google.maps.Marker({
          position: {lat: review.geoloc.lat, lng: review.geoloc.lng},
          map: $scope.map,
          icon: image
        });
        $scope.mapMarkers[review.id].addListener('click', function() {
          $scope.infoMarkers[review.id].open($scope.map, $scope.mapMarkers[review.id]);
        });
      });
    };

    $scope.calculateCarouselReviews = function () {
      if ($scope.map) {
        var bounds = $scope.map.getBounds();
        if(bounds) {
          $scope.$apply(function () {
            $scope.carouselReviews = [];
            angular.forEach($scope.reviews, function (review) {
              var isInsideBounds = bounds.contains(new google.maps.LatLng(review.geoloc.lat, review.geoloc.lng));
              if (isInsideBounds) {
                $scope.carouselReviews.push(review);
              }
            });
          });
        }
      }
    };

    $scope.navigateToReview = function (reviewId) {
      $location.path('/app/reviews/' + reviewId);
    };

    $scope.moveMapTo = function (selected) {
      // calculate the new center of the map and assign it
      var coordinates = [];
      angular.forEach($scope.reviews, function (review) {
        if (review.zona === selected.name) {
          coordinates.push({
            latitude: review.geoloc.lat,
            longitude: review.geoloc.lng
          })
        }
      });
      var newCenter = geolib.getCenter(coordinates);

      // this one triggers a bounds change, which calls calculateCarouselReviews
      $scope.map.setCenter(new google.maps.LatLng(newCenter.latitude, newCenter.longitude));
    };

    /**
     * Filter popup related
     */

    $scope.showFilters = function () {
      $scope.filterPopup = $ionicPopup.show({
        title: 'Filtrar',
        templateUrl: 'templates/popups/filters.html',
        cssClass: 'filters-popup',
        scope: $scope
      });
    };

    $scope.hideFilters = function () {
      $scope.filterPopup.close();
      $scope.selectedFilters();
    };

    $scope.filters = FilterService.filters;

    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };

    $scope.selectedFilters = function () {
      $scope.filtros = 0;
      var tipoCom = $scope.filters.tipoComida.values;
      var tipoOca = $scope.filters.tipoOcasion.values;
      var zona = $scope.filters.zona.values;
      tipoCom.forEach(function (tipo) {
        if(tipo.selected){
          $scope.filtros ++;
        }
      });
      tipoOca.forEach(function (tipo) {
        if(tipo.selected){
          $scope.filtros ++;
        }
      });
      zona.forEach(function (tipo) {
        if(tipo.checked){
          $scope.filtros ++;
        }
      });
    };

    $scope.clearFilters = function () {
      var tipoCom = $scope.filters.tipoComida.values;
      var tipoOca = $scope.filters.tipoOcasion.values;
      var zona = $scope.filters.zona.values;
      tipoCom.forEach(function (tipo) {
        tipo.selected = false;
      });
      tipoOca.forEach(function (tipo) {
        tipo.selected = false;
      });
      zona.forEach(function (tipo) {
        tipo.checked = false;
      });
    };

    $scope.markSelected = function (name, catName) {
      var cat = $scope.filters[catName].values;
      cat.forEach(function (cat) {
        if(cat.nombre != name){
          cat.selected = false;
        }
      })
    }
  })

  .controller('FavoritosCtrl', function ($scope, DataService, $ionicLoading) {

    $ionicLoading.show({
      template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
    });

    $scope.isFavorites = true;

    $scope.$on('$ionicView.enter', function (e) {
      Promise.all([DataService.getFavorites(), DataService.getNewsfeed()])
        .then(function (res) {
          var favorites = res[0];
          var reviews = res[1].data.news;
          $scope.$apply(function () {
            $scope.favorites = [];
            for (var i = 0; i < reviews.length; i++) {
              var reviewId = reviews[i].id + "";
              if (favorites.indexOf(reviewId) >= 0) {
                $scope.favorites.push(reviews[i]);
              }
              $ionicLoading.hide();
            }
          });
        });
    });
  })

  .controller('ReviewCtrl', function ($scope, $stateParams, DataService, AuthService, $http, $ionicModal, $ionicPopup, $ionicLoading) {
    $scope.comentario = "";
    $scope.calificacion = false;

    $scope.onReadySwiper = function(swiper){
      swiper.initObservers();
    };

    $ionicModal.fromTemplateUrl('templates/modals/review-map.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.mapModal = modal;
      });

      $ionicModal.fromTemplateUrl('templates/modals/comments-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        $scope.commentsModal = modal;
      });

      $scope.newComment = function(com){
        $ionicLoading.show({
          template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
        });
        var comentario = {
          id_restaurante: $scope.review.id,
          nombre_usuario: JSON.parse(window.localStorage['profile']).name,
          fecha: new Date(),
          comentario: com
        };
        DataService.postComment(comentario).then(function (res) {
          $ionicPopup.alert({
            title: 'Comentario guardado',
            template: 'Gracias! Tu comentario a sido agregado.'
          });
          $scope.commentGet();
          $ionicLoading.hide();
        });
        $scope.closeComments();
      };

      $scope.openComments = function () {
        $scope.commentsModal.show();
      };

      $scope.closeComments = function () {
        $scope.commentsModal.hide();
      };

      $scope.openMap = function () {
        $scope.mapModal.show();
        $ionicLoading.show({
          template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
        });

      var image = {
        url: 'img/PinMap.png',
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(40, 51),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 51)
      };

      var mapOptions = {
        zoom: 15,
        center: {lat: $scope.review.geoloc.lat, lng: $scope.review.geoloc.lng},
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          mapTypeIds: ['roadmap', 'terrain']
        }
      };

      $scope.map = null;
      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      var marker = new google.maps.Marker({

        position: {lat: $scope.review.geoloc.lat, lng: $scope.review.geoloc.lng},
        map: $scope.map,
        icon: image
      });

      $ionicLoading.hide();
    };


      $scope.closeMap = function () {
        $scope.mapModal.hide();
      };

      $scope.isLocalFavorite = function (reviewId) {
        return DataService.isLocalFavorite(reviewId);
      };
    $scope.review ={};
      $scope.review.fotos=[];
      DataService.getReview($stateParams.reviewId).then(function (res) {
        $scope.commentGet();
        var e = res.data;
        e.calUsersTotal = (e.calUsers.reduce(function (prev, curr) {
          return prev + curr;
        }, 0) / e.calUsers.length).toFixed(1);

        e.priceString = "$".repeat(e.precio);

        $scope.review = e;
      });

      $scope.commentGet = function () {
        DataService.getComments($stateParams.reviewId).then(function (com) {
          console.log("comentarios", com);
          $scope.comments = com.data.comentarios;
          angular.forEach($scope.comments, function (com) {
            var fechaCom = new Date(com.fecha);
            var current = new Date();
            if(fechaCom.getDay() < current.getDay()){
              com.fecha = (current.getDay() - fechaCom.getDay()) + " d"
            }
            else if(fechaCom.getHours() < current.getHours()) {
              com.fecha = (current.getHours() - fechaCom.getHours()) + " h";
            }
            else{
              com.fecha = (current.getMinutes() - fechaCom.getMinutes()) + " min";
            }
          })
        })
      };


      $scope.sliderOptions = {
        range: {min: 0, max: 10},
        step: 0.1,
        start: 5
      };

      $scope.sliderEventHandlers = {
        set: function (values, handle, unencoded) {
          $scope.userGrade = parseFloat(values);
        }
      };

      $scope.isLoggedIn = function () {
        return AuthService.isLoggedIn();
      };

      $scope.isAdmin = function () {
        return window.localStorage.tipo=="admin";
      };

      $scope.toggleFavorite = function (reviewId) {
        if($scope.isLoggedIn()) {
          DataService.toggleFavorite(reviewId);
        } else {
          $ionicPopup.alert({
            template: 'Para seleccionar favoritos debes iniciar sesión',
            cssClass: 'filters-popup',
            title: 'Iniciar sesión',
            okType: 'button-balanced',
            okText: "Cerrar"
          });
        }
      };

      $scope.isFavorite = function (reviewId) {
        return DataService.isFavorite(reviewId);
      };

      $scope.sendUserGrade = function () {
        $ionicLoading.show({
          template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
        });
        var user = $scope.review.platos.find(function (user, index) {
          if(user == JSON.parse(window.localStorage['profile']).name){
            $scope.index = index;
            return true;
          }
        });
        if(user){
          var reviewWithNewGrade = {};
          reviewWithNewGrade.calUsers = $scope.review.calUsers;
          reviewWithNewGrade.platos = $scope.review.platos;
          reviewWithNewGrade.calUsers[$scope.index] = $scope.userGrade;
          reviewWithNewGrade.platos.push(JSON.parse(window.localStorage['profile']).name);
          $http.put('https://app.bogotaeats.com/api/restaurantes/'+$scope.review.id, reviewWithNewGrade)
            .then(function (res) {
              $ionicLoading.hide();
              $scope.review.calUsersTotal = (res.data.calUsers.reduce(function (prev, curr) {
                return prev + curr;
              }, 0) / res.data.calUsers.length).toFixed(1);

              $ionicPopup.alert({
                title: 'Calificación guardada',
                template: 'Gracias! Tu calificación ha sido guardada.'
              });
            });
        }else{
          var reviewWithNewGrade = {};
          reviewWithNewGrade.calUsers = $scope.review.calUsers;
          reviewWithNewGrade.platos = $scope.review.platos;
          reviewWithNewGrade.calUsers.push($scope.userGrade);
          reviewWithNewGrade.platos.push(JSON.parse(window.localStorage['profile']).name);
          $http.put('https://app.bogotaeats.com/api/restaurantes/'+$scope.review.id, reviewWithNewGrade)
            .then(function (res) {
              $ionicLoading.hide();
              $scope.review.calUsersTotal = (res.data.calUsers.reduce(function (prev, curr) {
                return prev + curr;
              }, 0) / res.data.calUsers.length).toFixed(1);

              $ionicPopup.alert({
                title: 'Calificación guardada',
                template: 'Gracias! Tu calificación ha sido guardada.'
              });
            });
        }
      };

      $scope.options = {
        loop: false,
        effect: 'fade',
        speed: 500
      };

      // $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      //   // data.slider is the instance of Swiper
      //   $scope.review=data;
      //   $scope.review.fotos = data.slider;
      // });
      //
      // $scope.$on("$ionicSlides.slideChangeStart", function(event, data){
      //   console.log('Slide change is beginning');
      // });
      //
      // $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
      //   // note: the indexes are 0-based
      //   $scope.activeIndex = data.slider.activeIndex;
      //   $scope.previousIndex = data.slider.previousIndex;
      // });


  })

  .controller('adminCtrl', function ($scope, $state, $ionicHistory) {

  })

  .controller('reviewsCtrl', function ($ionicModal, $ionicPopup, $scope, $ionicHistory, FilterService, $stateParams, DataService, $cordovaCamera, $ionicLoading, $state, $location, $cordovaGeolocation) {

    $scope.onReadySwiper = function(swiper){
      swiper.initObservers();
    };

    $scope.cord = {latitud: 4.6713176, longitude: -74.0477429};

    $cordovaGeolocation.getCurrentPosition().then(function (res) {
      // body...
      $scope.cord = res.coords;
    })

    $ionicModal.fromTemplateUrl('templates/modals/review-map.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function (modal) {
      $scope.mapModal = modal;
    })
    $scope.myGoBack = function() {
      $ionicHistory.goBack();
    };
    $scope.filters = FilterService.filters;
    $scope.foto = false;
    $scope.fotos = [];

    if($stateParams.idReview){
      DataService.getReview($stateParams.idReview)
        .then(function (data) {
          $scope.review = data.data;
          $scope.fotos = $scope.review.fotos;
          $scope.foto = true;
          $scope.review.precio = "$".repeat($scope.review.precio);
          $scope.review.tipoComida.forEach(function (tipoCom) {
            $scope.filters.tipoComida.values.find(function (filtTipo) {
              if(filtTipo.nombre == tipoCom){
                filtTipo.selected = true;
              }
            });
          });
          $scope.review.tipoOcasion.forEach(function (tipoOca) {
            $scope.filters.tipoOcasion.values.find(function (filtTipo) {
              if(filtTipo.nombre == tipoOca){
                filtTipo.selected = true;
              }
            });
          });
          $scope.filters.zona.values.find(function (filtzona) {
            if(filtzona.name == $scope.review.zona){
              filtzona.checked = true;
            }
          })
        });
      $scope.updating = true;
    }

    $scope.review = {
      nombre: null,
      zona: null,
      direccion: null,
      geoloc: {
        lat: null,
        lng: null,
      },
      comentario: null,
      fecha: new Date(),
      precio: null,
      calificacion: null,
      calUsers:[],
      fotos:[],
      platos:[
        {}
      ],
      tipoComida: [],
      tipoOcasion: []
    };

    $scope.picture = function () {
      var options = {
        quality: 50,
        targetWidth: 400,
        targetHeight: 400,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.CAMERA,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.img = imageData;
        var fot = "data:image/jpeg;base64," + imageData;
        $scope.fotos.push(fot);
        $scope.foto = true;
      }, function (err) {
      });
    };

    $scope.selectPicture = function () {
      var options = {
        quality: 50,
        targetWidth: 400,
        targetHeight: 400,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.img = imageData;
        var fot = "data:image/jpeg;base64," + imageData;
        $scope.fotos.push(fot);
        $scope.foto = true;
      }, function (err) {
        console.log(err);
      });
    };


    Array.prototype.move = function (from, to) {
      this.splice(to, 0, this.splice(from, 1)[0]);
    };

    $scope.placeFirst = function (index) {
      $scope.fotos.move(index, 0);
    };

    $scope.eliminarFoto = function (index) {
      $scope.fotos.splice(index, 1);
    };

    $scope.openMap = function () {
      $scope.mapModal.show();
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });

      var image = {
        url: 'img/PinMap.png',
        // This marker is 20 pixels wide by 32 pixels high.
        size: new google.maps.Size(40, 51),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 51)
      };
      if($scope.review.geoloc.lat===null && $scope.review.geoloc.lng===null){
        var mapOptions = {
            zoom: 15,
            center: {lat: $scope.cord.latitude, lng: $scope.cord.longitude},
            mapTypeControl: true,
            mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            mapTypeIds: ['roadmap', 'terrain']
        }
      };
      } else{

      var mapOptions = {
        zoom: 15,
          center: {lat: $scope.review.geoloc.lat, lng: $scope.review.geoloc.lng},
        mapTypeControl: true,
        mapTypeControlOptions: {
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
          mapTypeIds: ['roadmap', 'terrain']
        }
      };}

      $scope.map = null;
      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
      var marker;
      if($scope.review.geoloc.lat===null && $scope.review.geoloc.lng===null){
         marker = new google.maps.Marker({
          draggable:true,
          position: {lat:$scope.cord.latitude, lng: $scope.cord.longitude},
          map: $scope.map,
          icon: image
        });
      } else{

        marker = new google.maps.Marker({
          draggable:true,
          position: {lat:$scope.review.geoloc.lat, lng:$scope.review.geoloc.lng},
          map: $scope.map,
          icon: image
        });}

      google.maps.event.addListener(marker, 'dragend', function (event) {
        lat = this.getPosition().lat();
        lng = this.getPosition().lng();
        document.getElementById("lat").value=lat;
        document.getElementById("lng").value=lng;
        $scope.review.geoloc.lat=lat;
        $scope.review.geoloc.lng=lng;
      });

      $ionicLoading.hide();
    };


    $scope.closeMap = function () {

      $scope.mapModal.hide();
    };



    $scope.saveReview = function (review) {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      review.fotos = $scope.fotos;
      var tipoComida = $scope.filters.tipoComida.values;
      var tipoOcasion = $scope.filters.tipoOcasion.values;
      var ubicacion = $scope.filters.zona.values;
      review.estado = "guardada";
      var arrOca = tipoOcasion.filter(function (tipo) {
        return tipo.selected == true;
      });
      var arrCom = tipoComida.filter(function (tipo) {
        return tipo.selected == true;
      });
      var arrZona = ubicacion.filter(function (tipo) {
        return tipo.checked == true;
      });
      arrOca.forEach(function (oca) {
        review.tipoOcasion.push(oca.nombre);
      });
      arrCom.forEach(function (com) {
        review.tipoComida.push(com.nombre);
      });
      arrZona.forEach(function (zona) {
        review.zona = zona.name;
      });
      review.precio = review.precio.length;
      DataService.newReview(review).then(function (res) {
        $ionicHistory.goBack();
        $ionicLoading.hide();
      }).catch(function (err) {
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error',
          template: 'Revisa los campos y vuelve a intentar'
        });
      });
    };

    $scope.publicarReview = function (review) {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      review.fotos = $scope.fotos;
      review.estado = "publicada";
      var tipoComida = $scope.filters.tipoComida.values;
      var tipoOcasion = $scope.filters.tipoOcasion.values;
      var ubicacion = $scope.filters.zona.values;
      var arrOca = tipoOcasion.filter(function (tipo) {
         return tipo.selected == true;
      });
      var arrCom = tipoComida.filter(function (tipo) {
        return tipo.selected == true;
      });
      var arrZona = ubicacion.filter(function (tipo) {
        return tipo.checked == true;
      });
      arrOca.forEach(function (oca) {
        review.tipoOcasion.push(oca.nombre);
      });
      arrCom.forEach(function (com) {
        review.tipoComida.push(com.nombre);
      });
      arrZona.forEach(function (zona) {
        review.zona = zona.name;
      });
      review.precio = review.precio.length;
      DataService.newReview(review).then(function (res) {
        $ionicHistory.goBack();
        $ionicLoading.hide();
      }).catch(function (err) {
        console.log(err);
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error',
          template: 'Revisa los campos y vuelve a intentar'
        });
      });
    };

    $scope.showFilters = function () {
      $scope.filterPopup = $ionicPopup.show({
        title: 'Categorias',
        templateUrl: 'templates/popups/filters.html',
        cssClass: 'filters-popup',
        scope: $scope
      })

    };

    $scope.toggleGroup = function (group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };
    $scope.isGroupShown = function (group) {
      return $scope.shownGroup === group;
    };

    $scope.filterByName = function (searchParam) {
      return function (item) {
        var name = item.nombre.toUpperCase();
        var search = searchParam.toUpperCase();
        return name.includes(search);
      }
    };
    $scope.hideFilters = function () {
      $scope.filterPopup.close();
    };


    $scope.change=function() // no ';' here
    {
      if ($scope.review.precio==null || $scope.review.precio=="" || $scope.review.precio=="$$$$" ){
      $scope.review.precio = "$";
      }
      else if ($scope.review.precio=="$"){
      $scope.review.precio = "$$";
      }
      else if ($scope.review.precio=="$$"){
        $scope.review.precio = "$$$";
      }
      else{
        $scope.review.precio = "$$$$";
      }
    }


    $scope.updateReview = function (review) {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      review.fotos = $scope.fotos;
      var tipoComida = $scope.filters.tipoComida.values;
      var tipoOcasion = $scope.filters.tipoOcasion.values;
      var ubicacion = $scope.filters.zona.values;
      var arrOca = tipoOcasion.filter(function (tipo) {
        return tipo.selected == true;
      });
      var arrCom = tipoComida.filter(function (tipo) {
        return tipo.selected == true;
      });
      var arrZona = ubicacion.filter(function (tipo) {
        return tipo.checked == true;
      });
      arrOca.forEach(function (oca) {
        review.tipoOcasion.push(oca.nombre);
      });
      arrCom.forEach(function (com) {
        review.tipoComida.push(com.nombre);
      });
      arrZona.forEach(function (zona) {
        review.zona = zona.name;
      });
      review.precio = review.precio.length;
      DataService.updateReview(review).then(function (res) {
        $ionicHistory.goBack();
        $ionicLoading.hide();
      }).catch(function (err) {
        console.log(err);
        $ionicLoading.hide();
        $ionicPopup.alert({
          title: 'Error',
          template: 'Revisa los campos y vuelve a intentar'
        });
      });
    }
  })

  .controller('editableCtrl', function ($scope, DataService, $location, $ionicPopup, FilterService, $ionicLoading, $http) {
    $ionicLoading.show({
      template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
    });

    $scope.selectedOrder = 'fecha';
    $scope.reverseOrder = true;

    DataService.getReviews().then(function (res) {
      $scope.reviews = res.data;
      $ionicLoading.hide();
    });

    $scope.showButtons = function (review) {
      $scope.reviewAEditar = review;
      $scope.menuEdicion = $ionicPopup.show({
        title: 'Menu',
        templateUrl: 'templates/popups/menuEdicion.html',
        cssClass: 'filters-popup',
        scope: $scope
      })
    };

    $scope.hideButtons = function () {
      $scope.menuEdicion.close();
    };

    $scope.onSwipe = function (direction) {
      console.log(direction);
    };

    $scope.editarReview = function () {
      $scope.menuEdicion.close();
      $location.path("/app/review/"+$scope.reviewAEditar.id);
    };

    $scope.publicarReview = function () {
      $http.put("http://http://app.bogotaeats.com//api/restaurantes/"+$scope.reviewAEditar.id, {estado: 'publicada'}).then(function (res) {
        load();
      });
    };

    $scope.OcultarReview = function () {
      $http.put("http://http://app.bogotaeats.com//api/restaurantes/"+$scope.reviewAEditar, {estado: 'oculta'}).then(function (res) {
        load();
      });
    };

  })

  .controller('bannersCtrl', function ($scope, DataService, $ionicLoading, $ionicHistory) {
    function loadBanners() {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      DataService.getBanners().then(function (res) {
        $ionicLoading.hide();
        $scope.banners = res.data;
      });
    }
    loadBanners();

    $scope.deleteBanner = function (id) {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      DataService.deleteBanner(id).then(function (res) {
        $ionicLoading.hide();
        loadBanners();
      })
    }

  })

  .controller('newBannersCtrl', function ($scope, DataService, $ionicLoading, $ionicHistory, $cordovaCamera) {

    $scope.banner = {id_banner:0};

    $scope.agregarBanner = function (banner) {
      $ionicLoading.show({
        template: '<img src="img/BE_Loading_White.gif" width="50%"> <p>Loading</p>'
      });
      DataService.agregarBanner(banner).then(function () {
        $ionicHistory.goBack();
        $scope.banner = {id_banner:0};
        $ionicLoading.hide();
      })
    };

    $scope.picture = function () {
      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.img = imageData;
        var fot = "data:image/jpeg;base64," + imageData;
        $scope.banner.foto = fot;
        $scope.foto = true;
      }, function (err) {
        console.log(err);
      });
    };

    $scope.selectPicture = function () {
      var options = {
        quality: 50,
        targetWidth: 400,
        targetHeight: 400,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit: true,
        encodingType: Camera.EncodingType.JPEG,
        popoverOptions: CameraPopoverOptions,
        saveToPhotoAlbum: false,
        correctOrientation: true
      };

      $cordovaCamera.getPicture(options).then(function (imageData) {
        $scope.img = imageData;
        var fot = "data:image/jpeg;base64," + imageData;
        $scope.banner.foto = fot;
        $scope.foto = true;
      }, function (err) {
        console.log(err);
      });
    };
  });
