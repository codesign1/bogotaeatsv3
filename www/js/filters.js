
angular.module('bogotaEats.controllers')
.filter('bogotaEats', function (FilterService) {
  return function(reviews) {
    if(reviews) {
      var atLeastOneChecked = false;
      var filteredReviews = reviews.filter(function(review) {
          var overOcasion = false;
          var overTipo = false;
          var overLoc = false;
          var zonas = 0;
          var tipos = 0;
          var ocasion = 0;
          var ocaTipo = false;
          var tipoLoc = false;
          var ocaLoc = false;
          var mult = false;
          angular.forEach(FilterService.filters, function(value, categoryName) {
            for(var i = 0; i < value.values.length; i++) {
              atLeastOneChecked = atLeastOneChecked || value.values[i].selected || value.values[i].checked;
              if(review[categoryName].indexOf(value.values[i].nombre)>=0) {
                if(categoryName == "tipoComida"){
                  overTipo = overTipo || value.values[i].selected;
                }
                else{
                  overOcasion = overOcasion || value.values[i].selected;
                }
              }
              if(review[categoryName].indexOf(value.values[i].name)>=0){
                overLoc = overLoc || value.values[i].checked;
              }
              if(value.values[i].selected){
                if(categoryName == "tipoComida"){
                  tipos++;
                }else{
                  ocasion++;
                }
              }
              if(value.values[i].checked){
                zonas++;
              }
              if(zonas > 0 && tipos > 0 && ocasion > 0){
                mult = true;
              }
              else if(zonas > 0 && tipos > 0 ){
                tipoLoc = true;
              }
              else if(ocasion > 0 && tipos > 0 ){
                ocaTipo = true;
              }
              else if(zonas > 0 && ocasion > 0 ){
                ocaLoc = true;
              }
            }
          });
          if(mult){
            return overOcasion && overTipo && overLoc;
          }else if(tipoLoc){
            return overLoc && overTipo;
          }else if(ocaTipo){
            return overOcasion && overTipo;
          }else if(ocaLoc){
            return overLoc && overOcasion;
          }else {
            return overOcasion || overTipo || overLoc;
          }
        }) || reviews;
      if(!atLeastOneChecked || filteredReviews.length === 0) {
        if(atLeastOneChecked){
          return filteredReviews;
        }
        else{
          return reviews;
        }
      }
      return filteredReviews;
    }
    else {
      return reviews;
    }
  }
});
