//// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js

var AUTH0_CLIENT_ID = 'R1hg70wpo2mtJqaYmNITvd6J2IKQ6W3O';
var AUTH0_CALLBACK_URL = location.href;
var AUTH0_DOMAIN = 'bogotaeats.auth0.com';
angular.module('bogotaEats', ['ionic', 'bogotaEats.controllers', 'bogotaEats.services', 'auth0.auth0', 'angular-jwt',
  'ya.nouislider', 'ngCordova', 'ksSwiper'])

  .run(['$ionicPlatform', 'AuthService', function ($ionicPlatform, AuthService) {

    AuthService.authenticateAndGetProfile();

    AuthService.checkAuthOnRefresh();

    $ionicPlatform.ready(function () {
      setTimeout(function() {
        navigator.splashscreen.hide();
      }, 100);
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

    });
  }])

  .config(function ($locationProvider, $stateProvider, $urlRouterProvider, angularAuth0Provider) {
    $locationProvider.hashPrefix('!');

    $stateProvider

      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.newsfeed', {
        url: '/newsfeed',
        views: {
          'menuContent': {
            templateUrl: 'templates/newsfeed.html',
            controller: 'NewsfeedCtrl'
          }
        }
      })

      .state('app.map', {
        url: '/map',
        views: {
          'menuContent': {
            templateUrl: 'templates/map.html',
            controller: 'MapCtrl'
          }
        }
      })

      .state('app.favoritos', {
        url: '/favoritos',
        views: {
          'menuContent': {
            templateUrl: 'templates/favoritos.html',
            controller: 'FavoritosCtrl'
          }
        }
      })

      .state('signup', {
        url: '/sign-up',
        templateUrl: 'templates/signup.html',
        controller: 'SignUpCtrl'
      })

      .state('signup2', {
        url: '/sign-up2',
        templateUrl: 'templates/signup2.html',
        controller: 'SignUp2Ctrl'
      })

      .state('app.review', {
        url: '/reviews/:reviewId',
        views: {
          'menuContent': {
            templateUrl: 'templates/review.html',
            controller: 'ReviewCtrl'
          }
        }
      })

      .state('app.admin', {
        url: '/admin',
        views: {
          'menuContent': {
            templateUrl: 'templates/admin/dashboard.html',
            controller: "adminCtrl"
          }
        }
      })

      .state('app.newReview', {
        url: '/review/:idReview',
        views: {
          'menuContent': {
            templateUrl: 'templates/admin/review.html',
            controller: 'reviewsCtrl'
          }
      }})

      .state('app.editableReviews', {
        url: '/editableReviews',
        views: {
          'menuContent': {
            templateUrl: 'templates/admin/editableReviews.html',
            controller: 'editableCtrl'
          }
        }
        })
      .state('app.banners',{
        url: "/banners",
        views: {
          'menuContent': {
            templateUrl: 'templates/admin/banners.html',
            controller: 'bannersCtrl'
          }
        }
        })
        .state('app.newBanner',{
          url: "/banners/newBanner",
          views: {
            'menuContent': {
              templateUrl: 'templates/admin/newBanner.html',
              controller: 'newBannersCtrl'
            }
          }
        });

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/newsfeed');

    angularAuth0Provider.init({
      clientID: AUTH0_CLIENT_ID,
      domain: AUTH0_DOMAIN
    });
  });
