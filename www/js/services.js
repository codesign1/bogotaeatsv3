angular.module('bogotaEats.services', [])
.factory('DataService', function ($http, $q) {

  var reviews;

  var localFavoritesList = [];

  var favoritesId;

  return {
    toggleFavorite: function (reviewId) {
      reviewId = reviewId + "";
      if (this.isLocalFavorite(reviewId)) {
        for (var i = 0; i < localFavoritesList.length; i++) {
          if (localFavoritesList[i] === reviewId) {
            localFavoritesList.splice(i, 1);
            break;
          }
        }
      } else {
        localFavoritesList.push(reviewId + "");
      }

      if (localFavoritesList.length > 0) {
        this.patchFavorites();
      } else {
        this.deleteFavorites();
      }
    },

    deleteFavorites: function () {
      $http.get('https://app.bogotaeats.com/api/favoritos?filter[where][idUser]='
        + JSON.parse(localStorage.getItem('profile')).email)
      .then(function (res) {
        return res.data[0];
      })
      .then(function (res) {
        $http.delete('https://app.bogotaeats.com/api/favoritos/' + res.id);
      });
    },

    isLocalFavorite: function (reviewId) {
      reviewId = reviewId + "";
      return localFavoritesList.reduce(function (prev, curr) {
        return prev || curr === reviewId;
      }, false);
    },

    isFavorite: function (reviewId) {
      // $http.get('http://app.bogotaeats.com/api/favoritos/' + reviewId).then(function (res) {
      //   console.log(res);
      //   return res;
      // });
      // return this.getFavorites().then(function(res) {
      //   return $scope.favorites.indexOf(reviewId >= 0);
      // });
    },

    patchFavorites: function () {
      if (favoritesId) {
        $http.patch('https://app.bogotaeats.com/api/favoritos/', {
          idUser: JSON.parse(localStorage.getItem('profile')).email,
          restaurantes: localFavoritesList,
          id: favoritesId
        });
      } else {
        $http.get('https://app.bogotaeats.com/api/favoritos?filter[where][idUser]='
          + JSON.parse(localStorage.getItem('profile')).email)
        .then(function (res) {
          return res.data[0];
        })
        .then(function (res) {
          if (res) {
            favoritesId = res.id;
            $http.patch('https://app.bogotaeats.com/api/favoritos/', {
              idUser: JSON.parse(localStorage.getItem('profile')).email,
              restaurantes: localFavoritesList,
              id: res.id
            });
          } else {
            $http.patch('https://app.bogotaeats.com/api/favoritos/', {
              idUser: JSON.parse(localStorage.getItem('profile')).email,
              restaurantes: localFavoritesList,
            });
          }
        });
      }
    },

    getFavorites: function () {
      var profile = JSON.parse(localStorage.getItem('profile'));
      if (profile) {
        return $http.get('https://app.bogotaeats.com/api/favoritos?filter[where][idUser]='
          + profile.email).then(function (res) {
          if (res.data.length > 0) {
            localFavoritesList = res.data[0].restaurantes;
            favoritesId = res.data[0].id;
            return res.data[0].restaurantes;
          } else {
            return [];
          }
        });
      } else {
        return [];
      }
    },

    getNewsfeed: function () {
      return $http.get('https://app.bogotaeats.com/api/restaurantes/newsfeed');
    },

    getReviews: function () {
      return $http.get('https://app.bogotaeats.com/api/restaurantes');
    },
    getReview: function (reviewId) {
      return $http.get('https://app.bogotaeats.com/api/restaurantes/' + reviewId);
    },
    newReview: function (review) {
      return $http.post('https://app.bogotaeats.com/api/restaurantes/', review);
    },
    updateReview: function (review) {
      return $http.put('https://app.bogotaeats.com/api/restaurantes/', review);
    },
    getComments: function (restaurante) {
      return $http.get('https://app.bogotaeats.com/api/comentarios/comentariosRestaurante?id_restaurante='+restaurante);
    },
    postComment: function (comment) {
      return $http.post('https://app.bogotaeats.com/api/comentarios', comment);
    },
    getAmountReviews: function () {
      return $http.get("https://app.bogotaeats.com/api/restaurantes/count");
    },
    getBannersActuales: function () {
      return $http.get("https://app.bogotaeats.com/api/banners/BannerActual");
    },
    getBanners: function () {
      return $http.get("https://app.bogotaeats.com/api/banners");
    },
    deleteBanner: function (id) {
      return $http.delete("https://app.bogotaeats.com/api/banners/"+id);
    },
    agregarBanner: function (banner) {
      return $http.post("https://app.bogotaeats.com/api/banners", banner);
    }
  }
})

.factory('DistanceService', function () {
  return {
    getDistance: function (lat1, lon1, lat2, lon2, unit) {
      var radlat1 = Math.PI * lat1 / 180;
      var radlat2 = Math.PI * lat2 / 180;
      var theta = lon1 - lon2;
      var radtheta = Math.PI * theta / 180;
      var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
      dist = Math.acos(dist);
      dist = dist * 180 / Math.PI;
      dist = dist * 60 * 1.1515;
      if (unit == "K") {
        dist = dist * 1.609344
      }
      if (unit == "N") {
        dist = dist * 0.8684
      }
      return dist;
    }
  }
})

.factory('FilterService', function ($ionicPopup, DataService) {
  // filters object contains static categories
  var filters = {
    tipoComida: {
      name: "Tipo de Comida",
      values: [
        {nombre: "Americana", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-01.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-01.png", selected: false},
        {nombre: "Asiatica", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-02.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-02.png", selected: false},
        {nombre: "Cafe", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-03.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-03.png", selected: false},
        {nombre: "Carne", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-04.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-04.png", selected: false},
        {nombre: "Rapida", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-05.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-05.png", selected: false},
        {nombre: "Española", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-06.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-06.png", selected: false},
        {nombre: "Francesa", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-07.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-07.png", selected: false},
        {nombre: "Hamburguesas", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-08.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-08.png", selected: false},
        {nombre: "Italiana", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-09.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-09.png", selected: false},
        {nombre: "Mediterranea", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-10.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-10.png", selected: false},
        {nombre: "Mexicana", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-11.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-11.png", selected: false},
        {nombre: "Peruana", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-12.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-12.png", selected: false},
        {nombre: "Pizza", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-13.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-13.png", selected: false},
        {nombre: "Saludable", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-14.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-14.png", selected: false},
        {nombre: "Sanduche", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-15.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-15.png", selected: false},
        {nombre: "Sushi", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-16.png", imgSel: "img/filter" +
        "-icons/ComidasWhite/BE_ComidasWhite-16.png", selected: false},
        {nombre: "No Convencional", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-17.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-17.png", selected: false},
        {nombre: "Fusión", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-18.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-18.png", selected: false},
        {nombre: "Mar", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-19.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-19.png", selected: false},
        {nombre: "Colombiana", img: "img/filter-icons/ComidasBlack/BE_ComidasBlack-20.png", imgSel: "img/filter-icons/ComidasWhite/BE_ComidasWhite-20.png", selected: false}
      ]
    },
    tipoOcasion: {
      name: "Ocasión",
      values: [
        {nombre: "Alta cocina", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-01.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-01.png", selected: false},
        {nombre: "Buena Terraza", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-02.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-02.png",  selected: false},
        {nombre: "En los rines", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-03.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-03.png",  selected: false},
        {nombre: "Desayunos", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-04.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-04.png",  selected: false},
        {nombre: "Cumpleaños", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-05.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-05.png",  selected: false},
        {nombre: "Domingo Abuela", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-06.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-06.png",  selected: false},
        {nombre: "De mes", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-07.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-07.png",  selected: false},
        {nombre: "Para el gringo", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-08.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-08.png",  selected: false},
        {nombre: "Primera cita", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-09.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-09.png",  selected: false},
        {nombre: "Rapida", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-10.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-10.png",  selected: false},
        {nombre: "Grado", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-11.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-11.png",  selected: false},
        {nombre: "Clandestino", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-12.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-12.png",  selected: false},
        {nombre: "Tarjeta corporativa", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-13.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-13.png",  selected: false},
        {nombre: "Trago y picadas", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-14.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-14.png",  selected: false},
        {nombre: "Precopeo", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-15.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-15.png",  selected: false},
        {nombre: "Oficinero", img: "img/filter-icons/OcasionesBlack/BE_Ocaciones_Black-16.png", imgSel: "img/filter-icons/OcasionesWhite/BE_Ocaciones_White-16.png",  selected: false}
      ]
    },
    zona: {
      name: "Ubicación",
      values: [
        {name: "Usaquén", checked: false},
        {name: "Zona G", checked: false},
        {name: "Macarena", checked: false},
        {name: "Zona T", checked: false},
        {name: "85", checked: false},
        {name: "Usaquen", checked: false},
        {name: "Candelaria", checked: false},
        {name: "Chapinero", checked: false},
        {name: "Quinta Camacho", checked: false},
        {name: "93", checked: false},
        {name: "World Trade Center", checked: false},
        {name: "La 90", checked: false}
      ]
    }
  };

  // DataService.getReviews().then(function(reviews) {
  //   angular.forEach(reviews, function(review) {
  //     angular.forEach(filters, function(obj, category) {
  //       obj.values[review[category]] = false;
  //     });
  //   });
  // });

  return {
    filters: filters
  }
})

.factory('AuthService', function ($rootScope, angularAuth0, authManager, jwtHelper, $location, $ionicPopup, $ionicHistory, $state) {
  var userProfile = JSON.parse(localStorage.getItem('profile')) || {};

  function login(username, password) {
    angularAuth0.login({
      connection: 'Username-Password-Authentication',
      responseType: 'token',
      email: username,
      password: password
    }, onAuthenticated, null);
  }

  function signup(username, password, callback) {
    console.log('signing up');
    angularAuth0.signup({
      connection: 'Username-Password-Authentication',
      responseType: 'token',
      email: username,
      password: password
    }, onAuthenticated, null);
  }

  function loginWithGoogle() {
    angularAuth0.login({
      connection: 'google-oauth2',
      responseType: 'token',
      // popup: true
    }, onAuthenticated, null);
  }

  function loginWithFacebook() {
    angularAuth0.login({
      connection: 'facebook',
      responseType: 'token',
      // popup: true
    }, onAuthenticated, null);
  }

  // Logging out just requires removing the user's
  // id_token and profile
  function logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    localStorage.removeItem('authorized');
    authManager.unauthenticate();
    userProfile = {};
  }

  function authenticateAndGetProfile() {
    var result = angularAuth0.parseHash(window.location.hash);

    if (result && result.idToken) {
      onAuthenticated(null, result);
    } else if (result && result.error) {
      onAuthenticated(result.error);
    }
  }

  function onAuthenticated(error, authResult) {
    if (error) {
      return $ionicPopup.alert({
        title: 'Login failed!',
        template: error
      });
    }

    localStorage.setItem('id_token', authResult.idToken);
    authManager.authenticate();

    angularAuth0.getProfile(authResult.idToken, function (error, profileData) {
      if (error) {
        return console.log(error);
      }

      console.log("aut");


      localStorage.setItem('profile', JSON.stringify(profileData));
      userProfile = profileData;

      // set authorized, which is used for non-logged in users
      localStorage.setItem('authorized', 'true');

      // make sure there's not a back button on redirect
      $ionicHistory.clearHistory();
      if(profileData.user_metadata){
        if(profileData.user_metadata.roles[0] == "admin"){
          localStorage.setItem('tipo', profileData.user_metadata.roles[0]);
          console.log("admin");
          $state.go('app.admin');
        }
      }
      else {
        localStorage.setItem('tipo', "user");
        console.log("user");
        $state.go('app.newsfeed');
      }

    });
  }

  function checkAuthOnRefresh() {
    var token = localStorage.getItem('id_token');
    if (token) {
      if (!jwtHelper.isTokenExpired(token)) {
        if (!$rootScope.isAuthenticated) {
          authManager.authenticate();
        }
      }
    }
  }

  function isLoggedIn() {
    return !!localStorage.getItem('id_token');
  }

  return {
    login: login,
    logout: logout,
    signup: signup,
    loginWithGoogle: loginWithGoogle,
    loginWithFacebook: loginWithFacebook,
    checkAuthOnRefresh: checkAuthOnRefresh,
    authenticateAndGetProfile: authenticateAndGetProfile,
    isLoggedIn: isLoggedIn
  }
})
;
